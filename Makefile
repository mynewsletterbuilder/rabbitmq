.PHONY: docker

IMAGE_BASE = jbanetwork/
IMAGE = rabbitmq
MY_PWD = $(shell pwd)

all: docker

docker:
	docker build -t $(IMAGE_BASE)$(IMAGE) -f $(MY_PWD)/Dockerfile $(MY_PWD)
ifneq (,$(findstring p,$(MAKEFLAGS)))
	docker push $(IMAGE_BASE)$(IMAGE)
endif

run:
	docker run -it --rm $(IMAGE_BASE)$(IMAGE) bash