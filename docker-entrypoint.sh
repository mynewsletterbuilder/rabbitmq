#!/bin/bash

cookieFile="/var/lib/rabbitmq/.erlang.cookie"
echo "$RABBITMQ_ERLANG_COOKIE" > "$cookieFile"
chmod 600 "$cookieFile"
chown rabbitmq:rabbitmq "$cookieFile"

configFile="/etc/rabbitmq/rabbitmq.config"
sed -i "s/#RABBITMQ_IP#/${RABBITMQ_IP}/g" ${configFile};
sed -i "s/#RABBITMQ_PORT#/${RABBITMQ_PORT}/g" ${configFile};
sed -i "s/#RABBITMQ_DEFAULT_PASS#/${RABBITMQ_DEFAULT_PASS}/g" ${configFile};
sed -i "s/#RABBITMQ_DEFAULT_USER#/${RABBITMQ_DEFAULT_USER}/g" ${configFile};

if [ $1 = "rabbitmq-server" ] && [ $RABBITMQ_CLUSTER_WITH ] && [ ! -f /var/lib/rabbitmq/.CLUSTERED ]; then
    echo "clustering with: ${RABBITMQ_CLUSTER_WITH}"
    sleep 5  # eventuall we should try rabbitmqctl -n rabbit@mm-rabbitmq-master cluster_status
    rabbitmq-server &
    sleep 2
    rabbitmqctl stop_app
    rabbitmqctl join_cluster ${RABBITMQ_CLUSTER_WITH}
    rabbitmqctl start_app
    rabbitmqctl set_policy ha-all "" '{"ha-mode":"all","ha-sync-mode":"automatic"}'
    touch /var/lib/rabbitmq/.CLUSTERED
    tail -F /dev/null
    exit
fi

exec "$@"
